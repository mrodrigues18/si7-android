class User {
  String id;
  String pseudo;
  String fonction;
  String mdpUtilisateur;
  String titre;

  User({this.id, this.pseudo, this.fonction, this.mdpUtilisateur, this.titre});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      pseudo: json['pseudo'],
      fonction: json['fonction'],
      mdpUtilisateur: json['mdpUtilisateur'],
      titre: json['titre']
    );
  }
}