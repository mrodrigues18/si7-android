import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:si7/fragments/account_fragment.dart';
import 'package:si7/fragments/computer_fragment.dart';
import 'package:si7/fragments/index_fragment.dart';
import 'package:si7/fragments/script_fragment.dart';
import 'package:si7/fragments/employee_fragment.dart';
import 'package:si7/fragments/os_fragment.dart';
import 'package:si7/fragments/user_fragment.dart';
import 'package:si7/main.dart';
import 'package:si7/pages/login_page.dart';
import 'package:si7/pages/sign_in_page.dart';
import 'package:flutter/material.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class HomePage extends StatefulWidget {
  final drawerItems = [
    new DrawerItem("Accueil", Icons.home),
    new DrawerItem("Scripts", Icons.insert_drive_file),
    new DrawerItem("Utilisateurs", Icons.people),
    new DrawerItem("Employés", Icons.people),
    new DrawerItem("Ordinateurs", Icons.laptop),
    new DrawerItem("Systèmes d\'exploitations", Icons.language),
    new DrawerItem("Mon compte", Icons.account_circle),
    new DrawerItem("Se déconnecter", Icons.clear),
  ];

  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new IndexScreen();
      case 1:
        return new ScriptScreen();
      case 2:
        return new UserScreen();
      case 3:
        return new EployeeScreen();
      case 4:
        return new ComputerScreen();
      case 5:
        return new OSScreen();
      case 6:
        return new AccountScreen();
      case 7:
        signOutGoogle();
        FlutterError.onError = (FlutterErrorDetails details) {
          FlutterError.dumpErrorToConsole(details);
          if (kReleaseMode)
            exit(1);
        };
        Navigator.push(context, MaterialPageRoute(
            builder: (context) {return new LoginPage(); }
        )
        );
        break;
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(
          new ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            selected: i == _selectedDrawerIndex,
            onTap: () {
              Navigator.pop(context);
              _onSelectItem(i);
            },
          )
      );
    }

    return new Scaffold(
      appBar: new AppBar(
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: ListView(
        children: <Widget>[
          new Card(
            child: new Column(
              children: <Widget>[
                new UserAccountsDrawerHeader(
                  accountName: new Text(
                      getFullName()
                  ),
                  accountEmail: new Text(
                      getEmail()
                  ),
                  currentAccountPicture: new Image.network(
                      getImageUrl()
                  ),
                ),
                new Column(children: drawerOptions)
              ],
            ),
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}