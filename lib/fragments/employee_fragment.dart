import 'package:flutter/material.dart';
import 'package:si7/fragments/employees_pages/list_employees.dart';

class EployeeScreen extends StatefulWidget {
  EployeeScreen({Key key}) : super(key: key);

  @override
  _EmployeeScreenState createState() => _EmployeeScreenState();
}

class _EmployeeScreenState extends State {
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return new EmployeeListPage();
        break;
      default:
        return new Text("");
    }
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _getDrawerItemWidget(0),
    );
  }
}