import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AddScript extends StatefulWidget {
  @override
  AddAScript createState(){
    return AddAScript();
  }
}

class AddAScript extends State<AddScript>{
  final _formKey = GlobalKey<FormState>();
  final scriptNameController = TextEditingController();
  final scriptDescController = TextEditingController();
  final scriptVersionController = TextEditingController();
  List<DropdownMenuItem<int>> osList = [];

  int _selectedOS;

  void loadOSList(){
    osList = [];
    osList.add(new DropdownMenuItem(
      child: Text("Windows"),
      value: 1,
    ));
    osList.add(new DropdownMenuItem(
      child: Text("Linux"),
      value: 2,
    ));
    osList.add(new DropdownMenuItem(
      child: Text("Mac OS"),
      value: 3,
    ));
  }

  @override
  void dispose() {
    scriptNameController.dispose();
    scriptDescController.dispose();
    scriptVersionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    loadOSList();
    return Form(
      key: _formKey,
      child: Container(
        child: Column(
          children: <Widget>[
            Text(
              "Ajouter un script",
              style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 20,
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Nom du script',
                hintText: 'Nom du script',
              ),
              validator: (value) {
                if(value.isEmpty){
                  return 'Veuillez entrer une valeur valide.';
                }
                return null;
              },
              controller: scriptNameController,
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Description du script',
                hintText: 'Description du script',
              ),
              validator: (value) {
                if(value.isEmpty){
                  return 'Veuillez entrer une valeur valide.';
                }
                return null;
              },
              controller: scriptDescController,
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Version du script',
                hintText: 'Version du script',
              ),
              validator: (value) {
                if(value.isEmpty){
                  return 'Veuillez entrer une valeur valide.';
                }
                return null;
              },
              controller: scriptVersionController,
            ),
            DropdownButton(
              hint: new Text("Quel OS ?"),
              items: osList.toList(),
              value: _selectedOS,
              onChanged: (value){
                setState(() {
                  _selectedOS = value;
                });
              },
              isExpanded: true,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                onPressed: (){

                  if(_formKey.currentState.validate()){
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text("Téléversement des données pour " + scriptNameController.text)));
                  }
                },
                child: Text('Envoyer'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}