import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class AddOS extends StatefulWidget {
  @override
  AddAnOS createState(){
    return AddAnOS();
  }
}

class AddAnOS extends State<AddOS>{
  final _formKey = GlobalKey<FormState>();
  String message = '';
  TextEditingController osNameController = TextEditingController();

  Future uploadOS() async {
    final uri = 'http://serveur1.arras-sio.com/symfony4-4017/parcinformatique/web/index.php?page=AddOSJSON&nomOs=' + osNameController.text ;
    var response = await http.get(uri);

    if(response.statusCode == 200){
      final item = json.decode(response.body);

      message = item['message'];
      return Scaffold.of(context).showSnackBar(SnackBar(content: Icon(Icons.check), backgroundColor: Colors.green, duration: Duration(seconds: 3),));
    }
    return Scaffold.of(context).showSnackBar(SnackBar(content: Icon(Icons.clear), backgroundColor: Colors.red, duration: Duration(seconds: 3),));
  }

  @override
  void dispose() {
    osNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        child: Column(
          children: <Widget>[
            Text(
              "Ajouter un système d'exploitation",
              style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 20,
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Nom du système d'exploitation ?",
                hintText: "Nom du système d'exploitation ?",
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Veuillez entrer une valeur valide.';
                }
                return null;
              },
              controller: osNameController,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                onPressed: () {
                  if(_formKey.currentState.validate()){
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text("Téléversement des données pour " + osNameController.text), duration: Duration(seconds: 3),));
                  }
                  uploadOS();
                },
                child: Text('Envoyer'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}