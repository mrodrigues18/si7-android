import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ScriptPage extends StatelessWidget{
  final String fileContents;
  ScriptPage({Key key, @required this.fileContents}) : super(key: key);

  Future<String> fetchScripts() async {
    String theScript = fileContents;
    String url = 'http://serveur1.arras-sio.com/symfony4-4017/parcinformatique/src/scripts/' + theScript;

    var response = await http.get(url);

    if(response.statusCode == 200) {
      String listScripts = response.body;

      print(listScripts);
      return listScripts;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: Text("Script: " + fileContents),),
      body: new Container(
        child: new Center(
          child: new FutureBuilder(
            future: fetchScripts(),
            builder: (context, snapshot){
              if(!snapshot.hasData) return Center(
                child: CircularProgressIndicator(),
              );

              return ListView(
                children: <Widget>[
                  Card(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        new Text(
                          snapshot.data ?? 'Fichier introuvable',
                          softWrap: true,
                          style: TextStyle(
                              color: Colors.white
                          ),
                        )
                      ],
                    ),
                    color: Colors.black,
                  )
                ],
              );
            },
            /* Méthodes qui lit les fichiers présents dans le sous-dossier 'scripts'
            future: DefaultAssetBundle.of(context).loadString("assets/scripts/$fileContents"),
            builder: (context, snapshot){
              return new Text(
                snapshot.data ?? 'Fichier introuvable',
                softWrap: true,
                style: TextStyle(
                    color: Colors.red
                ),
              );
            },
            */
          ),
        ),
        color: Colors.black,
      ),
    );
  }
}