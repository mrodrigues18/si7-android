import 'dart:io';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:si7/pages/login_page.dart';

class GoodBye extends StatefulWidget{
  GoodBye({Key key}) : super(key: key);
  
  @override
  _GoodByeState createState() => _GoodByeState();
}

class _GoodByeState extends State<GoodBye> {
  void main(){
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if(kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Image.asset('assets/logo.png', width: 150, height: 150,),
                  new RaisedButton(child: Text("Retourner sur l'écran de connexion"), onPressed: () {Navigator.of(context).push(MaterialPageRoute(builder: (context) {return new LoginPage();}));}),
                  new RaisedButton(child: Text("Fermer l'application"), onPressed: () {Navigator.of(context).push(MaterialPageRoute(builder: (context) {Exit();}));},),
                ],
              ),
              new Flushbar(
                title: 'A bientôt...',
                message: 'Votre désinscription a bien été prise en compte.',
                icon: Icon(
                  Icons.info_outline,
                  size: 28,
                  color: Colors.orange.shade300,
                ),
                leftBarIndicatorColor: Colors.orange.shade300,
                duration: Duration(seconds: 3),
              )..show(context),
            ],
          ),
        ),
      )
    );
  }
  
  void Exit(){
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }
}