import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si7/fragments/index_pages/canvas_computers.dart';

class IndexScreen extends StatefulWidget {
  IndexScreen({Key key}) : super(key: key);

  @override
  IndexFragment createState() => IndexFragment();
}

class IndexFragment extends State<IndexScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  static TextEditingController textEditingController = new TextEditingController();
  List<String> _networks = <String>['', '1', '2'];
  String _network = '';

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new FormField(
                    builder: (FormFieldState state) {
                      return new InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.network_cell),
                          labelText: 'Choisir l\'identifiant du réseau',
                        ),
                        isEmpty: _network == '',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _network,
                            isDense: true,
                            onChanged: (value) {
                              setState(() {
                                _network = value;
                                state.didChange(value);
                              });
                            },
                            items: _networks.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (value){
                      if(value == null || value == ''){
                        return 'Veuillez entrer une valeur valide.';
                      }
                      return null;
                    },
                  ),
                  new Container(
                      padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Afficher le réseau'),
                        onPressed: (){
                          if(_formKey.currentState.validate()){
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) => CanvasPage(theNetwork: _network,)
                            ));
                          }
                        },
                      )
                  ),
                ],
              )
          )
      ),
    );
  }
}