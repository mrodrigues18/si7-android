import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AccountSettingsPage extends StatefulWidget {
  AccountSettingsPage({Key key}) : super(key: key);

  @override
  _AccountSettingsPage createState() => _AccountSettingsPage();
}

class _AccountSettingsPage extends State<AccountSettingsPage> {

  void main(){
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if(kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Text("Langue "),
                  new Text("Francais"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}