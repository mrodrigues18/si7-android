import 'package:flutter/material.dart';
import 'package:si7/fragments/os_pages/add_os.dart';
import 'package:si7/fragments/os_pages/list_os.dart';

class OSScreen extends StatefulWidget {
  OSScreen({Key key}) : super(key: key);

  @override
  _OSScreenState createState() => _OSScreenState();
}

class _OSScreenState extends State {
  int _selectedDrawerIndex = 0;
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return new OSListPage();
      case 1:
        return new AddOS();
      default:
        return new Text("");
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedDrawerIndex,
        onTap: (int index) {
          setState(() {
            _selectedDrawerIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.list),
            title: new Text("Liste des OS"),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.note_add),
            title: new Text("Ajouter un OS"),
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}