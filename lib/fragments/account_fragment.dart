import 'package:flutter/material.dart';
import 'package:si7/fragments/account_pages/settings_account.dart';
import 'package:si7/fragments/account_pages/view_account.dart';
import 'package:si7/pages/sign_in_page.dart';

class AccountScreen extends StatefulWidget {
  AccountScreen({Key key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State {
  int _selectedDrawerIndex = 0;
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return AccountViewPage();
      case 1:
        return AccountSettingsPage();
      default:
        return new Text("");
    }
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedDrawerIndex,
        onTap: (int index){
          setState(() {
            _selectedDrawerIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.contacts),
            title: new Text(getFullName()),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.settings),
            title: new Text('Paramètres'),
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}