class Computer{
  String idOrdinateur;
  String ip;
  String mac;
  String reseau;
  String nomOs;
  String nomStatut;
  String unEmploye;

  Computer({this.idOrdinateur, this.ip, this.mac, this.reseau, this.nomOs, this.nomStatut, this.unEmploye});

  factory Computer.fromJson(Map<String, dynamic> json) {
    return Computer(
      idOrdinateur: json['idOrdinateur'],
      ip: json['ip'],
      mac: json['mac'],
      reseau: json['reseau'],
      nomOs: json['nomOs'],
      nomStatut: json['nomStatut'],
      unEmploye: json['unEmploye']
    );
  }
}