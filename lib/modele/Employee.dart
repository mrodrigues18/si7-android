class Employee{
  String idEmploye;
  String email;
  String nomEmploye;
  String prenomEmploye;
  String descriptionRole;

  Employee({this.idEmploye, this.email, this.nomEmploye, this.prenomEmploye, this.descriptionRole});

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      idEmploye: json['idEmploye'],
      email: json['email'],
      nomEmploye: json['nomEmploye'],
      prenomEmploye: json['prenomEmploye'],
      descriptionRole: json['descriptionRole'],
    );
  }
}