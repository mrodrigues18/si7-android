import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si7/fragments/computer_pages/list_all_computers.dart';
import 'package:si7/fragments/computer_pages/list_networksComputers.dart';
import 'package:si7/fragments/computer_pages/pdf_all_computers.dart';

class ComputerScreen extends StatefulWidget{
  ComputerScreen({Key key}) : super(key: key);

  @override
  _ComputerScreenState createState() => _ComputerScreenState();
}

class _ComputerScreenState extends State {
  int _selectedDrawerIndex = 0;
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return new AllComputersPage();
      case 1:
        return new NetworkListPage();
      case 2:
        return new AllComputersPdf();
      default:
        return new Text("");
    }
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedDrawerIndex,
        onTap: (int index){
          setState(() {
            _selectedDrawerIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
              icon: new Icon(Icons.list),
              title: new Text('Tous les PC')
          ),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.network_cell),
              title: new Text('Réseaux disponibles')
          ),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.picture_as_pdf),
              title: new Text('PDF'),
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}