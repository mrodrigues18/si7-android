import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:si7/fragments/users_pages/list_users.dart';

class UserScreen extends StatefulWidget {
  UserScreen({Key key}) : super(key: key);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State {
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return new UsersListPage();
        break;
      default:
        return new Text("");
    }
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _getDrawerItemWidget(0),
    );
  }
}
