class Script {

  String  idScript;
  String  nomScript;
  String  version;
  String  descScript;
  String  idOS;
  String  nomOS;
  String  fichierScript;

  Script({this.idScript, this.nomScript, this.version, this.descScript, this.idOS, this.nomOS, this.fichierScript});

  factory Script.fromJson(Map<String, dynamic> json){
    return Script(
        idScript: json['idScript'],
        nomScript: json['nomScript'],
        version: json['version'],
        descScript: json['descScript'],
        idOS: json['idOs'],
        nomOS: json['nomOs'],
        fichierScript: json['fichierScript']
    );
  }
}