import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si7/pages/login_page.dart';

void main() => runApp(Login());

class Login extends StatelessWidget {
  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(context){
    return new MaterialApp(
      title: 'SI7',
      theme: ThemeData(
        primaryColor: Colors.orange,
        brightness: Brightness.light,
        primarySwatch: Colors.orange,
        buttonColor: Colors.orange,
        bottomAppBarColor: Colors.orange,
        accentColor: Colors.orange,
      ),
      darkTheme: ThemeData(
        primaryColor: Colors.black12,
        brightness: Brightness.dark,
        primarySwatch: Colors.orange,
        buttonColor: Colors.deepOrangeAccent,
        bottomAppBarColor: Colors.deepOrangeAccent,
        accentColor: Colors.deepOrangeAccent,
      ),
      home: LoginPage(),
    );
  }
}