import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si7/fragments/other_pages/goodbye_page.dart';
import 'package:si7/pages/login_page.dart';
import 'package:si7/pages/sign_in_page.dart';

class AccountViewPage extends StatefulWidget {
  AccountViewPage({Key key}) : super(key: key);

  @override
  _AccountViewPage createState() => _AccountViewPage();
}

class _AccountViewPage extends State<AccountViewPage> {
  bool _isButtonRetrievePressed = false;
  bool _dispCircBarRetrieve = false;
  bool _isButtonDeletePressed = false;
  bool _dispCircBarDelete = false;
  String message;
  bool isOk;
  var goodBye = new GoodBye();

  void main(){
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if(kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new RaisedButton(
                    child: new Text("Récupérer mes informations"),
                    onPressed: _isButtonRetrievePressed ? null : () {
                      setState(() {
                        _isButtonRetrievePressed = !_isButtonRetrievePressed;
                        _dispCircBarRetrieve = !_dispCircBarRetrieve;
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Récupération de vos données..."), duration: Duration(seconds: 1),));
                        retrieveAccountData();
                      });
                    },
                  ),
                  _dispCircBarRetrieve ? new CircularProgressIndicator():new Container(),
                ],
              ),
              new Row(
                children: <Widget>[
                  new RaisedButton(
                    child: new Text("Supprimer mon compte"),
                    color: Colors.red,
                    onPressed: _isButtonDeletePressed ? null : (){
                      setState(() {
                        _isButtonDeletePressed = !_isButtonDeletePressed;
                        _dispCircBarDelete = !_dispCircBarDelete;
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Suppression de vos informations..."), duration: Duration(seconds: 1), backgroundColor: Colors.redAccent));
                        deleteAccount();
                      });
                    },
                  ),
                  _dispCircBarDelete ? new CircularProgressIndicator() : new Container(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future retrieveAccountData() async {
    final uri = 'http://serveur1.arras-sio.com/symfony4-4017/parcinformatique/web/index.php?page=retrieveAccountData&email=' + getEmail();
    var response = await http.get(uri);

    if(response.statusCode == 200){
      final item = json.decode(response.body);

      message = item['message'];
      isOk = item['isOk'];
      setState(() {
        _dispCircBarRetrieve = false;
      });
      if(isOk){
        return Scaffold.of(context).showSnackBar(SnackBar(content: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[Icon(Icons.check), Text(message)],), backgroundColor: Colors.green, duration: Duration(seconds: 3),));
      } else {
        return Scaffold.of(context).showSnackBar(SnackBar(content: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[Icon(Icons.clear), Text(message)],), backgroundColor: Colors.red, duration: Duration(seconds: 2),));
      }
    }
    setState(() {
      _dispCircBarRetrieve = false;
    });
    return Scaffold.of(context).showSnackBar(SnackBar(content: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[Icon(Icons.clear), Text(message)],), backgroundColor: Colors.red, duration: Duration(seconds: 3),));
  }

  Future deleteAccount() async {
    addUserToDb();
    final uri = 'http://serveur1.arras-sio.com/symfony4-4017/parcinformatique/web/index.php?page=deleteAccount&email=' + getEmail();
    var response = await http.get(uri);

    if(response.statusCode == 200){
      final item = json.decode(response.body);

      message = item['message'];
      isOk = item['isOk'];
      setState(() {
        _dispCircBarDelete = false;
      });
      if(isOk){
        setState(() {
          _isButtonRetrievePressed = true;
        });
        Navigator.of(context).push(MaterialPageRoute(builder: (context){return GoodBye();}));
      } else {
        setState(() {
          _isButtonRetrievePressed = false;
        });
        return Scaffold.of(context).showSnackBar(SnackBar(content: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[Icon(Icons.clear), Text(message)],), backgroundColor: Colors.red, duration: Duration(seconds: 3),));
      }
    }
    return Scaffold.of(context).showSnackBar(SnackBar(content: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[Icon(Icons.clear), Text(message)],), backgroundColor: Colors.red, duration: Duration(seconds: 3),));
  }
}