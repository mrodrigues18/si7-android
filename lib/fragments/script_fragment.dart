import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'scripts_pages/list_scripts.dart';
import 'scripts_pages/add_script.dart';

class ScriptScreen extends StatefulWidget{
  ScriptScreen({Key key}) : super(key: key);

  @override
  _ScriptScreenState createState() => _ScriptScreenState();
}

class _ScriptScreenState extends State {
  int _selectedDrawerIndex = 0;
  IconData icon;

  _getDrawerItemWidget(int pos){
    switch(pos){
      case 0:
        return new ScriptListPage();
      case 1:
        return new AddScript();
      default:
        return new Text("");
    }
  }
  
  @override
  void initState(){
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedDrawerIndex,
        onTap: (int index){
          setState(() {
            _selectedDrawerIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.list),
            title: new Text('Liste des scripts')
          ),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.note_add),
              title: new Text('Ajouter un script')
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}